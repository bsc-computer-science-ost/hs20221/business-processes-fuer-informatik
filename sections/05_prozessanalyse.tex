\section{Prozessanalyse}
Es gibt eine Reihe von Prinzipien und Techniken im Rahmen der qualitativen Prozessanalyse, welche in den Folgenden Unterkapiteln beschrieben sind.


Warum wir Prozesse analysieren:
\vspace{-0.35cm}
\begin{itemize}
	\item \textbf{Mängel:} Aufwand verursacht durch Nachbesserungen, Ausschuss und falschen Informationen
	\item \textbf{Überproduktion:} Mehr Produkte oder Dienstleistungen als benötigt oder zum falschen Zeitpunkt
	\item \textbf{Warten:} Vergeudete Zeit um auf den nächsten Schritt in einem Prozess zu warten
	\item \textbf{Nicht-genutzte Talente:} Ungenutzte Talente, Wissen und Fähigkeiten der Mitarbeiter
	\item \textbf{Transport:} Unnötige Bewegungen von Material und Produkten bspw. verursacht durch Fehlplanung
	\item \textbf{Inventur:} Überschüssige Produkte und unbearbeitete Materialien ohne Bestellung
	\item \textbf{Bewegung:} Unnötige Laufwege von Mitarbeiter und Bewegungen von Materialien
	\item \textbf{Extra-Bearbeitung:} Mehrarbeit oder höhere Qualität als vom Kunden gefordert wurde
\end{itemize}

\subsection{Verschwendungsanalyse}
Von Prozess-Start bis -Ende Ausschau nach \glqq Muda\grqq{} (=Verschwendung) und diese reduzieren.
\begin{figure}[H]
\center
\includegraphics[width=.7\textwidth]{verschwendungsanalyse}
\end{figure}

\subsection{Wertschöpfungsanalyse}
Unnötige Schritte in einem Prozess identifizieren und beseiteigen. Aktivitäten im Prozess werden in drei Typen kategorisiert:
\begin{description}
	\item[Value Adding (VA)] Aktivität generiert eine \underline{direkte Wertschöpfung für den Kunden}
	\item[Business Value Adding (BVA)] Aktivität generiert \underline{keine Wertschöpfung für den Kunden}, ist aber \underline{notwendig oder} \underline{nützlich für das Unternehmen}
	\item[Non-Value Adding (NVA)] Aktivität fällt in keine der andern beiden Kategorien $\rightarrow$ Verschwendung erkennen und eliminieren
\end{description}

\newpage

\paragraph{Erstellen einer Wertschöpfungsanalyse:} \hfill \\
\vspace{-0.6cm}
\begin{figure}[H]
\includegraphics[width=.8\textwidth]{wertschöpfungsanalyse}
\end{figure}

\subsection{Problemverzeichnis}
Schwächen eines Prozesses organisieren und dokumentieren, sowie die Auswirkungen der Schwächen sowohl in qualitativer als auch quantitativer Hinsicht aufzeigen. \\
Das Problemverzeichnis kann als Tabelle mit folgenden Zeilen dargestellt werden: Name des Problems, Priorität, Kurze Beschreibung, Daten und Annahmen, Qualitative Auswirkungen, Quantitative Auswirkungen.

\paragraph{Beispieleintrag aus Problemverzeichnis von Elektrofirma:} \hfill \\
\vspace{-0.6cm}
\begin{figure}[H]
\includegraphics[width=.6\textwidth]{problemverzeichnis}
\end{figure}


\subsection{Pareto-Analyse \& PICK-Chart}
\begin{itemize}
	\item Ziel der Pareto-Analyse liegt in der Priorisierung der identifizierten Probleme (Ursachen)
	\item Der dahinter liegende Gedanke: 80-20-Regel (oder eben Pareto-Prinzip)
	\item Als Ausgangspunkt / Input bietet sich hier das Problemverzeichnis an
	\item Vorgehen: \\
			\includegraphics[width=.85\textwidth]{pareto_vorgehen}
\end{itemize}


\paragraph{Beispiele:} \hfill \\
\vspace{-0.6cm}
\begin{minipage}{.48\textwidth}
\begin{figure}[H]
\includegraphics[width=\textwidth]{pareto_beispiel}
\caption{Beispiel einer Pareto-Analyse. Es wird jeweils nur eine \glqq Dimension\grqq{} (Kosten, Zeit, etc.) betrachtet}
\end{figure}
\end{minipage}
\hfill%
\begin{minipage}{.48\textwidth}
\begin{figure}[H]
\includegraphics[width=\textwidth]{pickchart_beispiel}
\caption{Beispiel eines PICK-Chart. Zusätzliche Dimension \glqq Umsetzbarkeit\grqq{} im Vergleich zur Pareto-Analyse}
\end{figure}
\end{minipage}

\subsection{Ursachenanalyse}
Mehrere Techniken, um Ursachenvon Problemen oder unerwünschten Ereignissen zu identifizieren und zu verstehen.

\subsubsection{Ursache-Wirkungs-Diagramm (Fishbone / Ishikawa)}
Zusammenhang zwischen gegebenem negativen Effekt und seinen möglichen Ursachen darstellen. Eine bekannte Kategorisierung sind die 6 M's:
\vspace{-0.35cm}
\begin{itemize}
	\item \textbf{Mensch:} Faktoren, die sich auf die beteiligten Personen beziehen, z.B. unzureichende Qualifikationen oder Konflikte am Arbeitsplatz.
	\item \textbf{Maschine:} Faktoren, die sich auf die verwendete Technologie beziehen, z.B. Systemabstürze oder ein Mangel an Funktionalität in Applikationen.
	\item \textbf{Milieu:} Faktoren, die sich aus dem Umfeld ergeben wo der Prozess ausgeführt wird, z.B. Kunden. Sie sind ausserhalb der Kontrolle des Unternehmens.
	\item \textbf{Material:} Faktoren wie Rohstoffe, Verbrauchsmaterialien oder Daten, die von den Aufgaben im Prozess als Input benötigt werden.
	\item \textbf{Methode:} Faktoren, die sich aus der Art und Weise ergeben wie Dinge im Unternehmen gehandhabt werden, z.B. Arbeitsweisen, Prozesse oder Strukturen.
	\item \textbf{Messung:} Faktoren, die sich auf Messungen oder Berechnungen des Prozesses beziehen.
\end{itemize}

\newpage

\paragraph{Erstellung eines Ursache-Wirkungs-Diagramms:} \hfill \\
\vspace{-0.6cm}
\begin{enumerate}
	\item \textbf{Problem formulieren:} Zum Beispiel \glqq Equipment bei der Lieferung abgelehnt\grqq
	\item \textbf{Einflussgrössen festlegen:} Zum Beispiel 6 M's
	\item \textbf{Haupt-/Nebenursachen formulieren:} Sammlung möglicher Ursachen, z.B. Brainstorming Session im Team
	\item \textbf{Ursachen einordnen \& priorisieren:} Ursachen den festgeletetn Einflussgrössen zuordnen
\end{enumerate}

\begin{figure}[H]
\center
\includegraphics[width=.8\textwidth]{fishbone_beispiel}
\caption{Beispiel eines Ursache-Wirkungs-Diagramms für gegebenes Problem}
\end{figure}

\subsubsection{Warum-Warum-Diagramm}
\begin{wrapfigure}{r}{0.45\textwidth}
\includegraphics[width=.45\textwidth]{warum-warum-diagramm}
\end{wrapfigure}
Problem als Wurzel eines Baum-Diagramms. Anschliessend die Frage nach dem 'Warum' wiederholen (erfahrungsgemäss 3 - 5 mal), bis die kausalen bzw. beitragenden Faktoren für ein Problem benannte werden konnten.
